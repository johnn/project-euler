# Project Euler Diary

I'm using this project for learning Common Lisp (and maths, as well, I guess), so it makes sense to
keep a diary of my thoughts as I go.

## 2021-03-27

### Refactoring, part 1 - `asdf`

I've migrated the contents of `project-euler-test.asd` into `project-euler.asd`, renaming the test system from `project-euler-test` to `project-euler/test` (see as example [here](https://stevelosh.com/blog/2018/08/a-road-to-common-lisp/#s34-projects) and the system it references [here](https://github.com/sjl/bobbin/blob/master/bobbin.asd)).

What this means in practice is that I have two system definitions in `project-euler.asd` now, the second one looking something like...

```lisp
(defsystem project-euler/test
  :depends-on (:project-euler
               :lisp-unit)
  :components ((:file "package.test")
               (:module "t"
                :serial t
                :components
                        ((:file "utilities")
                         (:file "problems")
                         ))))
```

The main thing to note is that there's a `/` in the system name, between `project-euler` and `test`:

> [ASDF treats systems with a forward slash in their name specially and knows to look for them in the asd file named with the text before the slash.](https://stevelosh.com/blog/2018/08/a-road-to-common-lisp/#s34-projects)

### Refactoring, part 2 - `package.lisp`, `package.test.lisp`

I previously had `package.lisp` files under `src/problems` and `src/utilities`, and I had package definitions in the relevant files under `t/`. I've now consolidated this, so all package definitions are either in `package.lisp` or `package.test.lisp` (both in system root). (I've also used the opportunity to start cleaning up header comments).

## 2021-03-25

### My continuing adventures in CL logging

I *did* like [`vom`](https://github.com/orthecreedence/vom), but I'm liking [`verbose`](https://shinmera.github.io/verbose/) more. I couldn't get `vom` to display function names, without something convoluted like...

```lisp
(defun foo (param)
  (let ((funcname "foo"))
    (vom:debug "~a: param: ~a" funcname param)))
```

```
  <DEBUG> [12:10:56] common-lisp-user - foo: param: 1
NIL
```

`verbose` doesn't entirely solve this, but does provide a neater solution:

```lisp
(defun foo (param)
  (v:debug :foo "param: ~a" param))
```

```
2021-03-25 12:12:10 [DEBUG] <FOO>: param: 1
NIL
```

(`verbose`'s `v` is shorter than `vom`'s `vom`, too. `verbose` is \<ahem\> less verbose).

The category (`:foo`) can be dotted into subtrees, and multiple categories are allowed:

```lisp
(defun foo (param)
  (v:debug '(:foo :function-in) "param: ~a" param)
  (setq param (+ 1 param))
  (v:debug :foo.post-add "param: ~a" param)
  (setq param (/ param 2))
  (v:debug '(:foo.post-divide :function-return) "param: ~a" param)
  param)
```

```
2021-03-25 12:13:57 [DEBUG] <FOO><FUNCTION-IN>: param: 1
2021-03-25 12:13:57 [DEBUG] <FOO.POST-ADD>: param: 2
2021-03-25 12:13:57 [DEBUG] <FOO.POST-DIVIDE><FUNCTION-RETURN>: param: 1
1
```

### Git news

In other news I've created branches for the unsolved problems. This involved committing unsolved problems, but meh. I've never been one for avoiding a commit, simply because a task is incomplete. Especially on toy projects like this. I suppose I should `git stash` but meh.

### Meta thoughts

This diary might be better located in GitLab's wiki or on Wordpress... I do quite like the freedom and low overhead of only having to edit a Markdown file, though.

## 2021-03-24

### Adapting to a lispy world

So far CL seems pretty straightforward. I'm adapting to Polish Notation pretty well (I like it). The
2-space indent thing seemed weird at first, but has grown on me. The 100-column line-length just seems *sensible*. (Also: haven't terminals with 132-column support been a thing since the late 1970s? Typing this in 2021 on my 4K, 67cm monitor...) Parentheses have been less of an issue than I expected - I read somewhere that Lisp is basically like Python, pay attention to the indentation and with a good IDE taking care of the brackets, the brackets can more or less be ignored.

Things that are still weird include `if` - though I suppose really it's similar to C or Java, but instead of a `{` we're using `(progn` instead. I've learned to always use brackets in C or Java, to avoid problems like...

```java
if (ctr < 10)
    System.out.println("Still below 10...")
    logger.debug("Below 10")    // Added logging, but this will break because no brackets!!!
else
    System.out.println("Reached 10.")
```

...adding a new statement and breaking the code because you forgot to add brackets. Possibly I need
to learn to always add `(progn`s?

`loop` is supposed to be weird, and there's a tendency to recommend `iter` instead. I *am* finding `loop` weird - but in a "new language" way, not in a bizarre way. It seems much more powerful than looping constructs in other languages, I guess because it's combining `for`, `repeat`, `while`, etc.

I feel like I'm making a good start in grokking lists, but I'm also aware I have a long way to go: macros, for example, aren't really on my learning to-do list yet.

The syntax for `let` will take a wee bit of getting use to, but it does seem fairly sensible - the brackets enclose the scope:

```lisp
(let ((x 1) (y 2))
  (print (+ x y))  ; returns 3
  (setq x (+ 1 x)
  (print (+ x y))  ; returns 4
)
; x and y are out of scope here, have no value, so this is an error:
(print (+ x y))    ; throws a wobbly
```

...and while I'm on the subject of `let`, `setq` (and other `set`s too) are also new and strange to me, and will take some getting used to.

### Package management and build tools

`QuickLisp` seems reasonable as a package-management tool (Lisp seems to call packages "systems", but to be fair I'm used to Java's concept of packages which map very closely on to CL's concept of packages). `asdf` seems reasonable as a build-tool. I'm still getting used to both of them, and for now I like the fact that they're distinct - I always found [Ant](http://ant.apache.org/) easier to get to grips with than [Maven](http://maven.apache.org/), partly because Maven included package-management.

### Unit testing

There doesn't seem to be - despite claims to the contrary - a de facto unit-testing library. I've been using [`lisp-unit`](https://github.com/OdonataResearchLLC/lisp-unit), which is alright. It's easy enough to use, but its reports leave much to be desired. It defaults to telling you not much, counts of fails but no indication where the failure occurred. Even drilling down (with `(print-failures *)`) is less helpful than it could be. I do like - in fact I'm loving - the ability to tag tests, run groups of tests sliced and diced as needed. `lisp-unit` is one of the libraries suggested as the de facto unit-testing library. Another is [`FiveAM`](https://common-lisp.net/project/fiveam/), and I'm considering giving that a whirl.

### Logging

It's well established that there's no de facto logging library! And, as far as I can see, there isn't something like ~~Jakarta's~~ Apache's [`commons.logging`](https://commons.apache.org/proper/commons-logging/) which would be used as a wrapper around a concrete logger. Worse, there doesn't seem to be agreement on how loggers should function - to my (Java-infused) mind all loggers should have similar log-levels and be able to log to a diverse range of environments (`stdout`, an RDBMS, a file, `SYSLOG`, etc). Some CL loggers don't seem to support log-levels, etc. ~~The comfortingly-named [`log4cl`](https://github.com/sharplispers/log4cl/) seems like the best bet for now, and I'm considering giving it a whirl.~~  Right now my code is testament to `(format t "")` being CL's equivalent to `printf` or `System.out.println`...! Update: I'm liking [`vom`](https://github.com/orthecreedence/vom) more than `log4cl` - it's small and uses the `format t` idiom, so it's trivial to copy and replace `format t` => `vom:debug`. And its name is 🤮 chunderlightful.

### Managing unsolved problems

In terms of Project Euler itself, I assumed that I'd tackle one problem at a time, committing after each problem was solved. In practice I've now started three different problems that remain unsolved. Two are outwith Git, one has been committed. I need to rethink my Git workflow; specifically, I need to branch for each new problem I start. I really should have been doing this from the get-go...

### IDE fun

IDE-wise I've been using Atom with [SLIMA](https://atom.io/packages/slima), and various other Atom packages that SLIMA recommended (`parinfer`, etc). It's been OK. Atom is pretty good at handling parentheses and letting me get on with stuff, but it does seem fragile at times - tabs currently don't seem to be working, no great hardship as I can hit space two times instead. But indenting blocks of code is now frustrating. And SLIMA itself seems to disconnect any time there's a (continuable) exception. There *is* a defacto IDE for Common Lisp (and Lisp in general?), and it's `emacs` with `SLIME`. I grew up with Unix boxes that had `vi` installed on a good day, and have put off learning `emacs` ever since. Maybe now's finally the time to learn?!
