(defpackage :coop.git.johnn.euler.utilities
    (:use :common-lisp)
    (:export
        :generate-nth-triangular-number
        :get-factors
        :get-prime-factors
        :power
        :divides-evenly-p
        :primep
        :factorial
        :palindromep
        :add-string-digits))

(defpackage :coop.git.johnn.euler.problems
  (:use :common-lisp :coop.git.johnn.euler.utilities)
  (:export
      :multiples-of-3-and-5
      :even-fibonacci
      :get-largest-palindrome-product
      :get-smallest-multiple
      :sum-square-difference
      :find-nth-prime
      :largest-product-in-a-series
      :sum-of-digits-of-2-to-the-power
      :find-special-pythagorean-triplet
      :get-english-number
      :count-letters-in-english-numbers
      :find-X
      :find-Y
      :rotate
      :sort-digits
      :increment-permutation))
