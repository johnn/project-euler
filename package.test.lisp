(defpackage :coop.git.johnn.euler.problems.tests
  (:use :common-lisp :lisp-unit :coop.git.johnn.euler.problems))

(defpackage :coop.git.johnn.euler.utilities.tests
  (:use :common-lisp :lisp-unit :coop.git.johnn.euler.utilities))
