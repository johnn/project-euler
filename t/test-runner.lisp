(asdf:load-system :project-euler)
(asdf:load-system :project-euler/test)
(setq *print-failures* t)
(setq *print-errors* t)
(in-package :coop.git.johnn.euler.utilities.tests)
(run-tests :all)
(in-package :coop.git.johnn.euler.problems.tests)
(run-tests :all)
(in-package :cl-user)
