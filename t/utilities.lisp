;;;; Purpose:  Project Euler utilities - tests
;;;; Filename: utilities.lisp
;;;; Author:   John Niven <john@webarch.coop>
;;;; Created:  18 March 2021
;;;; Modified: 27 March 2021 - refactored package definitions to `package.test.adf`.

(in-package :coop.git.johnn.euler.utilities.tests)

(define-test test-get-factors
  (:tag :factors :list)
  (assert-equal (get-factors 4) (list 1 2 4))
  (assert-equal (get-factors 9) (list 1 3 9))
  (assert-equal (get-factors 25) (list 1 5 25))
  (assert-equal (get-factors 49) (list 1 7 49)))

(define-test test-get-prime-factors
  (:tag :prime :factors :list)
  (assert-equal (get-prime-factors 4) (list 2))
  (assert-equal (get-prime-factors 9) (list 3))
  (assert-equal (get-prime-factors 25) (list 5))
  (assert-equal (get-prime-factors 49) (list 7))
  (assert-equal (get-prime-factors 60) (list 2 3 5)))

(define-test test-divides-evenly-p
  (:tag :division :predicate)
  (assert-true (divides-evenly-p 6 3))
  (assert-true (divides-evenly-p 9 3))
  (assert-true (divides-evenly-p 8 4))
  (assert-true (divides-evenly-p 10 5))
  (assert-true (divides-evenly-p 15 5))
  (assert-true (divides-evenly-p 10 (power 5 1)))
  (assert-false (divides-evenly-p 7 3))
  (assert-false (divides-evenly-p 9 4))
  (assert-false (divides-evenly-p 11 5)))

(define-test test-palindromep
  (:tag :palindrome :predicate)
  (assert-true (palindromep 11))
  (assert-true (palindromep 202))
  (assert-true (palindromep 3113))
  (assert-true (palindromep 41214))
  (assert-false (palindromep 12))
  (assert-false (palindromep 203))
  (assert-false (palindromep 3114))
  (assert-false (palindromep 41215)))

(define-test test-primep
  (:tag :prime :predicate)
  (assert-false (primep 1))
  (assert-true (primep 2))
  (assert-true (primep 3))
  (assert-false (primep 4))
  (assert-true (primep 5))
  (assert-true (primep 7))
  (assert-false (primep 9))
  (assert-false (primep 10))
  (assert-true (primep 11)))
