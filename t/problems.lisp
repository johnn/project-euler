;;;; Purpose:  Project Euler problems - tests
;;;; Filename: problems.lisp
;;;; Author:   John Niven <john@webarch.coop>
;;;; Created:  19 March 2021
;;;; Modified: 27 March 2021 - refactored package definitions to `package.test.adf`.

(in-package :coop.git.johnn.euler.problems.tests)

(define-test test-problem-1
  (:tag :problem-1)
  (assert-equal (multiples-of-3-and-5 10) 23))

(define-test test-problem-2
  (:tag :problem-2)
  (assert-equal (even-fibonacci 89) 44))

(define-test test-problem-4
  (:tag :problem-4)
  (assert-equal (get-largest-palindrome-product 99) 9009))

(define-test test-problem-5
  (:tag :problem-5)
  (assert-equal (get-smallest-multiple 10) 2520))

(define-test test-problem-6
  (:tag :problem-6)
  (assert-equal (sum-square-difference 10) 2640))

(define-test test-problem-7
  (:tag :problem-7)
  (assert-equal (find-nth-prime 6) 13))

(define-test test-problem-8
  (:tag :problem-8)
  (assert-equal (largest-product-in-a-series 4) 5832))

(define-test test-problem-9
  (:tag :problem-9)
  (assert-equal (find-special-pythagorean-triplet 12) 60))

(define-test test-problem-16
  (:tag :problem-16)
  (assert-equal (sum-of-digits-of-2-to-the-power 15) 26))

(define-test test-get-english-number
  (:tag :problem-17)
  (assert-equal (get-english-number 0) "zero")
  (assert-equal (get-english-number 1) "one")
  (assert-equal (get-english-number 2) "two")
  (assert-equal (get-english-number 3) "three")
  (assert-equal (get-english-number 4) "four")
  (assert-equal (get-english-number 5) "five")
  (assert-equal (get-english-number 6) "six")
  (assert-equal (get-english-number 7) "seven")
  (assert-equal (get-english-number 8) "eight")
  (assert-equal (get-english-number 9) "nine")
  (assert-equal (get-english-number 10) "ten")
  (assert-equal (get-english-number 11) "eleven")
  (assert-equal (get-english-number 12) "twelve")
  (assert-equal (get-english-number 13) "thirteen")
  (assert-equal (get-english-number 14) "fourteen")
  (assert-equal (get-english-number 15) "fifteen")
  (assert-equal (get-english-number 16) "sixteen")
  (assert-equal (get-english-number 17) "seventeen")
  (assert-equal (get-english-number 18) "eighteen")
  (assert-equal (get-english-number 19) "nineteen")
  (assert-equal (get-english-number 20) "twenty")
  (assert-equal (get-english-number 21) "twentyone")
  (assert-equal (get-english-number 30) "thirty")
  (assert-equal (get-english-number 40) "forty")
  (assert-equal (get-english-number 50) "fifty")
  (assert-equal (get-english-number 60) "sixty")
  (assert-equal (get-english-number 70) "seventy")
  (assert-equal (get-english-number 80) "eighty")
  (assert-equal (get-english-number 90) "ninety")
  (assert-equal (get-english-number 100) "onehundred")
  (assert-equal (get-english-number 101) "onehundredandone")
  (assert-equal (get-english-number 111) "onehundredandeleven")
  (assert-equal (get-english-number 121) "onehundredandtwentyone")
  (assert-equal (get-english-number 1000) "onethousand"))

(define-test test-problem-17
  (:tag :problem-17)
  (assert-equal 19 (count-letters-in-english-numbers 5)))

(define-test test-rotate
  (:tag :problem-24)
  (assert-equal (list 0 1 2 4 3) (rotate (list 0 1 2 3 4) 3 4)))

(define-test test-find-X
  (:tag :problem-24)
  (assert-equal 3 (find-X (list 0 1 2 3 4)))
  (assert-equal 0 (find-X (list 0 4 3 2 1)))

  (assert-equal 2 (find-X (list 0 1 2 5 4 3)))
  (assert-equal 2 (find-X (list 0 1 3 5 4 2)))

  (assert-equal 1 (find-X (list 0 1 2)))
  (assert-equal 0 (find-X (list 0 2 1)))
  (assert-equal 1 (find-X (list 1 0 2)))
  (assert-equal 0 (find-X (list 1 2 0)))
  (assert-equal 1 (find-X (list 2 0 1)))
  (assert-false (find-X (list 2 1 0)))

  (assert-equal 2 (find-X (list 0 1 2 3)))
  (assert-equal 1 (find-X (list 0 1 3 2)))
  (assert-equal 2 (find-X (list 0 2 1 3)))
  (assert-equal 1 (find-X (list 0 2 3 1)))
  (assert-equal 0 (find-X (list 0 3 2 1)))
  (assert-equal 2 (find-X (list 1 0 2 3)))
  (assert-equal 1 (find-X (list 1 0 3 2))))


(define-test test-find-Y
  (:tag :problem-24)
  (assert-equal 4 (find-Y (list 0 1 2 3 4)))
  (assert-equal 4 (find-Y (list 0 4 3 2 1)))

  (assert-equal 2 (find-Y (list 0 1 2)))
  (assert-equal 2 (find-Y (list 0 2 1)))
  (assert-equal 0 (find-Y (list 1 0 2)))
  (assert-equal 1 (find-Y (list 1 2 0)))
  (assert-equal 2 (find-Y (list 2 0 1)))
  (assert-false (find-Y (list 2 1 0)))

  (assert-equal 3 (find-Y (list 0 1 2 3)))
  (assert-equal 3 (find-Y (list 0 1 3 2)))
  (assert-equal 1 (find-Y (list 0 2 1 3)))
  (assert-equal 2 (find-Y (list 0 2 3 1)))
  (assert-equal 3 (find-Y (list 0 3 2 1)))
  (assert-equal 3 (find-Y (list 1 0 2 3)))
  (assert-equal 0 (find-Y (list 1 0 3 2))))


(define-test test-sort-digits
  (:tag :problem-24)
  (assert-equal (list 0 1 3 2 4 5) (sort-digits (list 0 1 3 5 4 2) 2)))

(define-test test-increment-permutation
  (:tag :problem-24)
  (assert-equal (list 1 0) (increment-permutation (list 0 1)))
  (assert-false (increment-permutation (list 1 0)))

  (assert-equal (list 0 2 1) (increment-permutation (list 0 1 2)))
  (assert-equal (list 1 0 2) (increment-permutation (list 0 2 1)))
  (assert-equal (list 1 2 0) (increment-permutation (list 1 0 2)))
  (assert-equal (list 2 0 1) (increment-permutation (list 1 2 0)))
  (assert-equal (list 2 1 0) (increment-permutation (list 2 0 1)))
  (assert-false (increment-permutation (list 2 1 0)))

  (assert-equal (list 0 1 3 2) (increment-permutation (list 0 1 2 3)))
  (assert-equal (list 0 2 1 3) (increment-permutation (list 0 1 3 2)))
  (assert-equal (list 0 2 3 1) (increment-permutation (list 0 2 1 3)))
  (assert-equal (list 0 3 1 2) (increment-permutation (list 0 2 3 1)))
  (assert-equal (list 1 0 2 3) (increment-permutation (list 0 3 2 1)))
  (assert-equal (list 1 0 3 2) (increment-permutation (list 1 0 2 3)))
  (assert-equal (list 1 2 0 3) (increment-permutation (list 1 0 3 2))))
