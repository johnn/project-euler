(defsystem project-euler
  :author "John Niven <john@webarch.coop>"
  :maintainer "John Niven <john@webarch.coop>"
  :license "GPL-3.0-only"
  :homepage "https://git.coop/johnn/project-euler"
  :version "0.1"
  :depends-on (:verbose)
  :components ((:file "package")
               (:module "src/problems"
                :depends-on ("src/utilities")
                :serial t
                :components (
                             (:file "problem-1")
                             (:file "problem-2")
                             (:file "problem-4")
                             (:file "problem-5")
                             (:file "problem-6")
                             (:file "problem-7")
                             (:file "problem-8")
                             (:file "problem-9")
                             (:file "problem-16")
                             (:file "problem-17")
                             (:file "problem-20")
                             (:file "problem-21")
                             (:file "problem-24")
                             ))
               (:module "src/utilities"
                :serial t
                :components ((:file "utilities"))))
  :description "My attempt at tackling Project Euler using (and learning) Common Lisp."
  :long-description
  #.(uiop:read-file-string
     (uiop:subpathname *load-pathname* "README.md")))

(defsystem project-euler/test
  :author "John Niven <john@webarch.coop>"
  :license "GPL-3.0-only"
  :depends-on (:project-euler
               :lisp-unit)
  :components ((:file "package.test")
               (:module "t"
                :serial t
                :components
                        ((:file "utilities")
                         (:file "problems")
                         ))))
