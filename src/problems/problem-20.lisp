;;; Factorial digit sum - Problem 20
;;; problem-20.lisp
;;; John Niven
;;; 21 March 2021
;;; SOLVED: 648

;; n! means n × (n − 1) × ... × 3 × 2 × 1
;; For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
;; and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
;; Find the sum of the digits in the number 100!

(in-package :coop.git.johnn.euler.problems)

(defun sandbox-20 ()
  (print "Expected result: 27")
  (sum-digits-in-factorial 10))

(defun problem-20 ()
  (print "Expected result: 648")
  (sum-digits-in-factorial 100))

(defun sum-digits-in-factorial (number)
  (add-string-digits (princ-to-string (factorial number))))
