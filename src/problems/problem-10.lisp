;;; Summation of primes - Problem 10
;;; problem-10.lisp
;;; John Niven
;;; 14 March 2021
;;; SOLVED: 142913828922

;; The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
;; Find the sum of all the primes below two million.

(defun primep (number)
;    (format t "~%~%primep: number: ~a" number)
    (if (<= number 1)
        (progn
;            (format t "~%primep: (<= number 1): ~a" (<= number 1))
            (return-from primep nil))
        (if (= number 2)
            (progn
;                (format t "~%primep: (= number 2): ~a" (= number 2))
                (return-from primep T))
            (if (= (mod number 2) 0)
                (progn
;                    (format t "~%primep: (= (mod number 2) 0): ~a" (= (mod number 2) 0))
                    (return-from primep nil))
                (progn
                    (setq counter 3)
                    (loop while (<= (* counter counter) number) do
                        (if (= (mod number counter) 0)
                            (return-from primep nil)
                            (setq counter (+ 2 counter))))
                    (return-from primep T))))))

(defun summation-of-primes (below)
    (setq sum 2)
    (loop for i from 3 to (- below 1) by 2 do
        (if (primep i)
            (progn
                (setq sum (+ sum i))
                (format t "~%i: ~a, sum: ~a" i sum))))



    sum)


(defun test-problem-10 ()
    (= (summation-of-primes 10) 17))


(defun problem-10 ()
    (summation-of-primes 2000000))
