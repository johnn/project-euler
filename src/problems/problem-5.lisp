;;; Smallest multiple - Problem 5
;;; problem-5.lisp
;;; John Niven
;;; 13 March 2021
;;; SOLVED: 232792560

;; 2520 is the smallest number that can be divided by each of the numbers from 1
;; to 10 without any remainder.
;; What is the smallest positive number that is evenly divisible by all of the
;; numbers from 1 to 20?

(in-package :coop.git.johnn.euler.problems)

(defun divisible-by-all-p (number bound)
  (let ((evenly-divisible T))
    (loop for i from 3 to (- bound 1) do  ; optimisation: no point doing 1, and all numbers are even so no point doing 2, and no point doing (bound).
      (setq evenly-divisible (divides-evenly-p number i))
      while evenly-divisible)
    evenly-divisible))

(defun get-smallest-multiple (inc)
  (let ((number inc))
    (loop until (divisible-by-all-p number inc) do
      (setq number (+ inc number)))  ; optimisation
;      (format t "~%~a ~a" inc number))
;      (format t "~%.,:.,:.,:.,:")
    number))

(defun sandbox-5 ()
  (print "Expected result: 2520")
  (get-smallest-multiple 10))

(defun problem-5 ()
  "Brute-forcing is *slow*."
  (get-smallest-multiple 20))
