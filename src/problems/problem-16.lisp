;;; Power digit sum - Problem 16
;;; problem-16.lisp
;;; John Niven
;;; 21 March 2021
;;; SOLVED: 1366

;; 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
;; What is the sum of the digits of the number 2^1000?

(in-package :coop.git.johnn.euler.problems)

(defun sandbox-16 ()
  (print "Expected result: 26")
  (sum-of-digits-of-2-to-the-power 15))

(defun problem-16 ()
  (print "Expected result: 1366")
  (sum-of-digits-of-2-to-the-power 1000))

(defun sum-of-digits-of-2-to-the-power (exponent)
  (add-string-digits (princ-to-string (power 2 exponent))))
