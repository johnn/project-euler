;;; Longest Collatz sequence - Problem 14
;;; problem-14.lisp
;;; John Niven
;;; 17 March 2021
;;; SOLVED: 837799

;; The following iterative sequence is defined for the set of positive integers:
;;    n → n/2 (n is even)
;;    n → 3n + 1 (n is odd)
;; Using the rule above and starting with 13, we generate the following sequence:
;;    13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
;; It can be seen that this sequence (starting at 13 and finishing at 1)
;; contains 10 terms. Although it has not been proved yet (Collatz Problem), it
;; is thought that all starting numbers finish at 1.
;; Which starting number, under one million, produces the longest chain?
;; NOTE: Once the chain starts the terms are allowed to go above one million.

(defun problem-14 ()
  (let ((funcname "problem-14") (current-length 0) (max-ctr 0) (max-length 0))
    (loop for i from 2 to 1000000 do
      (setq current-length (collatz i))
      (if (> current-length max-length)
        (progn
          (setq max-length current-length)
          (setq max-ctr i)))
      (format t "~%~a: i: ~a, ~a > ~a. MAX: ~a" funcname i max-length current-length max-ctr))))


(defun test-14 ()
  nil)

(defun sandbox-14 ()
  nil)

(defparameter *cache-hash* (make-hash-table))

(defun purge-cache ()
  (let ((funcname "purge-cache"))
    (format t "~%~a: Purging cache..." funcname)
    (loop for i from 2 to 1000000 do
      (remhash i *cache-hash*))
    (format t "~%~a: Purge completed." funcname)))

(defun collatz (number)
  (let ((funcname "collatz") (current-number number) (seq-length 0))
    (loop while (/= 1 current-number) do
;      (format t "~%~a: ~a" funcname current-number)

      (if (nth-value 1 (gethash current-number *cache-hash*))
        (progn
          (setq seq-length (+ seq-length (gethash current-number *cache-hash*)))
;          (format t "~%~a: ~a [~a => ~a] CACHE" funcname current-number number seq-length)
          (setq current-number 1))
        (progn
          (if (evenp current-number)
            (setq current-number (/ current-number 2))
            (setq current-number (+ (* 3 current-number) 1)))
          (setq seq-length (+ 1 seq-length)))))
;          (format t "~%~a: ~a [~a => ~a] nocache" funcname current-number number seq-length))))

    (setf (gethash number *cache-hash*) seq-length)
;    (format t "~%~a: ~a [~a => ~a] RETURN" funcname current-number number seq-length)
    seq-length))
