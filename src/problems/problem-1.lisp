;;; Multiples of 3 and 5 - Problem 1
;;; problem-1.lisp
;;; John Niven
;;; 10 March 2021
;;; SOLVED: 233168

;; If we list all the natural numbers below 10 that are multiples of 3 or 5, we
;; get 3, 5, 6 and 9. The sum of these multiples is 23.
;; Find the sum of all the multiples of 3 or 5 below 1000.

(in-package :coop.git.johnn.euler.problems)

(defun sandbox-1 ()
  (print "Expected result: 23")
  (multiples-of-3-and-5 10))

(defun problem-1 ()
  (print "Expected result: 233168")
  (multiples-of-3-and-5 1000))

(defun create-list (number list maximum inc)
  (if (<= (+ inc number) maximum)
    (create-list (+ inc number) (cons (+ inc number) list) maximum inc)
    list))

(defun multiples-of-3-and-5 (below)
  (let ((sum 0) (lastx 0))
    (dolist (x (sort (create-list 0 (create-list 0 (cons 0 ()) (- below 1) 3) (- below 1) 5) #'<))
      (unless (eql x lastx)
        (progn
          (setf sum (+ sum x))
          (setf lastx x))))
    sum))
