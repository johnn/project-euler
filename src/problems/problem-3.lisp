;;; Largest prime factor - Problem 3
;;; problem-3.lisp
;;; John Niven
;;; 12 March 2021 - 14 March 2021
;;; SOLVED: 6857

;; The prime factors of 13195 are 5, 7, 13 and 29.
;; What is the largest prime factor of the number 600851475143 ?

(defun remove-multiples (number sieve)
  (setq return-sieve (list))
  (dolist (n sieve)
    (setq foo (pop sieve))
    (when (> (mod n number) 0)
      (push foo return-sieve)))
  (reverse return-sieve))

(defun sieve-of-eratosthenes (size)
  (setq sieve (list 2))
  (setq new-sieve (list))
  (format t "~%1: (length sieve): ~a" (length sieve))

  (loop for i from 3 to size by 2 do
    (format t "~%i: ~a / ~a" i size)
    (push i sieve))
  (setq sieve (reverse sieve))
  ;(format t "~%sieve: ~a" sieve)
  (format t "~%2: (length sieve): ~a" (length sieve))

  (loop while (> (length sieve) 0) do
    (setq current-value (car sieve))
    ;(format t "~%current-value: ~a" current-value)
    (push (pop sieve) new-sieve)
    (setq sieve (remove-multiples current-value sieve))
    (format t "~%3: (length sieve): ~a" (length sieve)))
  (reverse new-sieve))


(defun do-prime-factor-brute-force (number)

  ; Slightly optimised brute force solution:
  ; ----------------------------------------
  ; 1. Create `Sieve of Eratosthenes` containing primes up to number / 2
  ; 2. Iterate over primes in sieve, record primes where number divides by
  ;    prime modulo 0.
  ; 3. return highest prime

  ; This seems to work...
  (setq highest-prime 0)
  (dolist (n (sieve-of-eratosthenes (sqrt number)))
;      (format t "~%(= (mod ~a ~a) 0): ~a" number n (= (mod number n) 0))
;      (format t "~%number: ~a" number)
;      (format t "~%n: ~a" n)
    (if (= (mod number n) 0)
      (progn
;         (format t "~%YES n: ~a" n)
        (if (> n highest-prime)
          (progn
            (setq highest-prime n)
            (format t "~%HP1: highest-prime: ~a" highest-prime))))))

;           (format t "~%HP2: highest-prime: ~a" highest-prime)

;           (format t "~%NO  n: ~a" n)

  (format t "~%highest-prime: ~a" highest-prime)
  highest-prime)


(defun prime-factor ()
  (do-prime-factor 600851475143))


(defun test-prime-factor ()
  (print "Assert T")
  (equal (do-prime-factor 13195) 29))


(defun do-prime-factor (number)
  "https://www.mathblog.dk/project-euler-problem-3/"
  (setq new-number number)
  (setq largest-factor 0)
  (setq counter 2)

  (loop while (<= (* counter counter) new-number) do
    (if (= (mod new-number counter) 0)
      (progn
        (setq new-number (/ new-number counter))
        (setq largest-factor counter))
      (setq counter (+ 1 counter)))

    (when (> new-number largest-factor)
      (setq largest-factor new-number))))
