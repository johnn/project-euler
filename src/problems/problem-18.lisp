;;; Maximum path sum I - Problem 18
;;; problem-18.lisp
;;; John Niven
;;; 15 March 2021
;;; SOLVED: 1074

;; By starting at the top of the triangle below and moving to adjacent numbers
;; on the row below, the maximum total from top to bottom is 23.
;;    3
;;   7 4
;;  2 4 6
;; 8 5 9 3
;; That is, 3 + 7 + 4 + 9 = 23.
;; Find the maximum total from top to bottom of the triangle below:
;; ...[see code below]...
;; NOTE: As there are only 16384 routes, it is possible to solve this problem by
;; trying every route. However, Problem 67, is the same challenge with a
;; triangle containing one-hundred rows; it cannot be solved by brute force, and
;; requires a clever method! ;o)

(defun problem-triangle ()
    (list
            (list 75)
            (list 95 64)
            (list 17 47 82)
            (list 18 35 87 10)
            (list 20 04 82 47 65)
            (list 19 01 23 75 03 34)
            (list 88 02 77 73 07 63 67)
            (list 99 65 04 28 06 16 70 92)
            (list 41 41 26 56 83 40 80 70 33)
            (list 41 48 72 33 47 32 37 16 94 29)
            (list 53 71 44 65 25 43 91 52 97 51 14)
            (list 70 11 33 28 77 73 17 78 39 68 17 57)
            (list 91 71 52 38 17 14 91 43 58 50 27 29 48)
            (list 63 66 04 68 89 53 67 30 73 16 69 87 40 31)
            (list 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23)))

(defun test-triangle ()
    (list
            (list 3)
            (list 7 4)
            (list 2 4 6)
            (list 8 5 9 3)))

(defun test-triangle-1 ()
    (list
            (list 3)))

(defun test-triangle-2 ()
    (list
            (list 3)
            (list 7 4)))

(defun test-triangle-3 ()
    (list
            (list 3)
            (list 7 4)
            (list 2 4 6)))

(defun maximum-path-sum-1 (triangle)
    (elt (elt (prune-triangle triangle) 0) 0))

(defun problem-18 ()
    (maximum-path-sum-1 (problem-triangle)))

(defun test-problem-18 ()
    (print "Assert: T")
    (= (maximum-path-sum-1 (test-triangle)) 23))

;    3
;   7 4
;  2 4 6
; 8 5 9 3

(defun get-row (triangle row)
    (if (<= (length triangle) row)
        ()
        (elt triangle row)))

(defun get-value (triangle row col)
    (if (<= (length (get-row triangle row)) col)
        ()
        (elt (get-row triangle row) col)))

(defun get-children (triangle row col)
    (if (<= (length triangle) (+ 1 row))
        ()
        (list (get-value triangle (+ 1 row) col) (get-value triangle (+ 1 row) (+ 1 col)))))

(defun pick-higher (list)
;    (format t "~%pick-higher: list: ~a" list)
    (if (= (length list) 0)
        nil
        (progn
            (setq val0 (elt list 0))
            (setq val1 (elt list 1))
            (if (not val0) (setq val0 0))
            (if (not val1) (setq val1 0))
            (if (consp (elt list 0))
                (progn
                    (setq val0 (reduce '+ 'val0))))
            (if (consp (elt list 1))
                (progn
                    (setq val1 (reduce '+ 'val1))))
            (if (> val0 val1)
                (progn
                    (elt list 0))
                (progn
                    (elt list 1))))))

(defun get-higher-child (triangle row col)
    (pick-higher (get-children triangle row col))
)

; If (length triangle) = 1 then return triangle
; If (length triangle) = 2 then pick the highest child in row 1, and add it to row 0. Recurse this 1-row triangle.
; If (length triangle) >= 3 then
;       (1) create return-triangle containing all rows in triangle except the last 2.
;       (2) pick the highest child in the last row, and add it to the parent in the last row but one.
;       (3) Recurse return-triangle, which will have >1 rows.
(defun prune-triangle (triangle)
;    (format t "~%prune-triangle: triangle: ~a" triangle)
    (if (= (length triangle) 1)
        triangle
        (if (= (length triangle) 2)
            (progn
                (setq new-row nil)
                (push
                    (+
                            (get-value triangle 0 0)
                            (get-higher-child triangle 0 0))
                    new-row)
;                (format t "~%prune-triangle: (list new-row): ~a" (list new-row))
                (list new-row))
            (progn
                (setq return-triangle nil)
                (setq new-row nil)
                (loop for row from 0 to (- (length triangle) 3) do
                    (push (get-row triangle row) return-triangle))
                (loop for col from 0 to (- (length (get-row triangle (- (length triangle) 2))) 1) do
                    (push
                        (+
                                (get-value triangle (- (length triangle) 2) col)
                                (get-higher-child triangle (- (length triangle) 2) col))
                        new-row))
;                (format t "~%prune-triangle: (list (reverse new-row)): ~a" (list (reverse new-row)))
                (push (reverse new-row) return-triangle)
;                (format t "~%prune-triangle: (reverse (return-triangle)): ~a" (reverse return-triangle))
                (prune-triangle (reverse return-triangle))))))

(defun sandbox-18 ()

    (print "0.1 Original triangle:")
    (print (test-triangle))

    (print "1.1 These should return lists:")
    (format t "~%get-row(0): ~a" (get-row (test-triangle) 0))
    (format t "~%get-row(1): ~a" (get-row (test-triangle) 1))
    (format t "~%get-row(2): ~a" (get-row (test-triangle) 2))
    (format t "~%get-row(3): ~a" (get-row (test-triangle) 3))
    (print "1.2 These should return EMPTY lists:")
    ;(format t "~%get-row(0): ~a" (get-row (test-triangle) -1))
    (format t "~%get-row(4): ~a" (get-row (test-triangle) 4))

    (print "2.1 These should return atoms:")
    (format t "~%get-value(0, 0): ~a" (get-value (test-triangle) 0 0))
    (format t "~%get-value(1, 0): ~a" (get-value (test-triangle) 1 0))
    (format t "~%get-value(1, 1): ~a" (get-value (test-triangle) 1 1))
    (format t "~%get-value(3, 3): ~a" (get-value (test-triangle) 3 3))
    (print "2.2 These should return EMPTY lists:")
    ;(format t "~%get-value(0, 0): ~a" (get-value (test-triangle) -1 0))
    (format t "~%get-value(0, 1): ~a" (get-value (test-triangle) 0 1))
    (format t "~%get-value(3, 10): ~a" (get-value (test-triangle) 3 10))
    (format t "~%get-value(4, 0): ~a" (get-value (test-triangle) 4 0))

    (print "3.1 These should return lists:")
    (format t "~%get-children(0, 0): ~a" (get-children (test-triangle) 0 0))
    (format t "~%get-children(1, 0): ~a" (get-children (test-triangle) 1 0))
    (format t "~%get-children(1, 1): ~a" (get-children (test-triangle) 1 1))
    (print "3.2 These should return EMPTY lists:")
    (format t "~%get-children(3, 3): ~a" (get-children (test-triangle) 3 3))

    (print "4.1 These should return atoms:")
    (format t "~%get-higher-child(0, 0): ~a" (get-higher-child (test-triangle) 0 0))
    (format t "~%get-higher-child(1, 0): ~a" (get-higher-child (test-triangle) 1 0))
    (format t "~%get-higher-child(1, 1): ~a" (get-higher-child (test-triangle) 1 1))
    (print "4.2 These should return EMPTY lists:")
    (format t "~%get-higher-child(3, 3): ~a" (get-higher-child (test-triangle) 3 3))

    (print "5.0 Original triangle:")
    (print (test-triangle))

    (print "5.1 Pruned triangle 1:")
    (print (prune-triangle (test-triangle-1)))
    (print "5.2 Pruned triangle 2:")
    (print (prune-triangle (test-triangle-2)))
    (print "5.3 Pruned triangle 3:")
    (print (prune-triangle (test-triangle-3)))
    (print "5.4 Pruned triangle 4:")
    (print (prune-triangle (test-triangle)))

)
