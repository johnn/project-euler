;;; Factorial digit sum - Problem 21
;;; problem-21.lisp
;;; John Niven
;;; 21 March 2021
;;; SOLVED: 31626

;; Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly
;; into n.
;; If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are
;; called amicable numbers.
;; For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore
;; d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
;; Evaluate the sum of all the amicable numbers under 10000.

(in-package :coop.git.johnn.euler.problems)

;; No caching implemented yet...
(defparameter *hash-21* (make-hash-table))

(defun purge-cache-21 ()
  (let ((funcname "purge-cache"))
    (format t "~%~a: Purging cache..." funcname)
    (loop for i from 2 to 1000000 do
      (remhash i *hash-21*))
    (format t "~%~a: Purge completed." funcname)))

(defun sum-amicable-numbers (upper-bound)
  (let ((amicable-sum 0))
    (loop for i from 1 to upper-bound do
      (if (amicablep i)
        (progn
          (setq amicable-sum (+ i amicable-sum)))))
          ;(format t "~%~a is amicable! Sum is: ~a" i amicable-sum))))
    amicable-sum))

(defun get-proper-divisors (number)
  "Returns factors that are < `number`."
  (reverse (cdr (reverse (get-factors number)))))

(defun sum-proper-divisors (number)
  (reduce #'+ (get-proper-divisors number)))

(defun amicablep (number)
  (if (nth-value 1 (gethash number *hash-21*))
    T
    (and
        (= number (sum-proper-divisors (sum-proper-divisors number)))
        (/= number (sum-proper-divisors number)))))

(defun problem-21 ()
  (print "Expected result: 31626")
  (sum-amicable-numbers 10000))
