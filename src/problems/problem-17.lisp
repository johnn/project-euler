;;; Number letter counts - Problem 17
;;; problem-17.lisp
;;; John Niven
;;; 21 March 2021
;;; SOLVED: 21124

;; If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are
;; 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
;; If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many
;; letters would be used?
;; NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23
;; letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out
;; numbers is in compliance with British usage.

(in-package :coop.git.johnn.euler.problems)

(defun sandbox-17 ()
  (print "Expected result: 19")
  (count-letters-in-english-numbers 5))

(defun problem-17 ()
  (print "Expected result: 21124")
  (count-letters-in-english-numbers 1000))

(defun count-letters-in-english-numbers (number)
  (let ((count-of-letters 0))
    (loop for i from 1 to number do
      (setq count-of-letters (+ count-of-letters (length (get-english-number i)))))
      ;(format t "~%count-letters-in-english-numbers: i: ~a, eng: ~a, len: ~a, cnt: ~a" i (get-english-number i) (length (get-english-number i)) count-of-letters))
    count-of-letters))

(defun get-english-number (number)
  ;(format t "get-english-number (~a)" number)
  (if (= number 1000)
    "onethousand"
    (progn
      (if (>= number 100)
        (progn
          (if (= (mod number 100) 0)
            (format nil "~ahundred" (get-english-number (floor (/ number 100))))
            (format nil "~ahundredand~a" (get-english-number (floor (/ number 100))) (get-english-number (mod number 100)))))
        (progn
          (if (>= number 20)
            (get-english-number-20-99 number)
            (get-english-number-0-19 number)))))))

(defun get-english-number-0-19 (number)
  ;(format t "get-english-number-0-19 (~a)" number)
  (cond ((= number 0) "zero")
        ((= number 1) "one")
        ((= number 2) "two")
        ((= number 3) "three")
        ((= number 4) "four")
        ((= number 5) "five")
        ((= number 6) "six")
        ((= number 7) "seven")
        ((= number 8) "eight")
        ((= number 9) "nine")
        ((= number 10) "ten")
        ((= number 11) "eleven")
        ((= number 12) "twelve")
        ((= number 13) "thirteen")
        ((= number 14) "fourteen")
        ((= number 15) "fifteen")
        ((= number 16) "sixteen")
        ((= number 17) "seventeen")
        ((= number 18) "eighteen")
        ((= number 19) "nineteen")))

(defun get-english-number-20-99 (number)
  ;(format t "get-english-number-20-99 (~a)" number)
  (if (= (mod number 10) 0)
    (cond ((= number 20) "twenty")
          ((= number 30) "thirty")
          ((= number 40) "forty")
          ((= number 50) "fifty")
          ((= number 60) "sixty")
          ((= number 70) "seventy")
          ((= number 80) "eighty")
          ((= number 90) "ninety"))
    (format nil "~a~a" (get-english-number-20-99 (* (floor (/ number 10)) 10)) (get-english-number-0-19 (mod number 10)))))
