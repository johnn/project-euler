;;; Special Pythagorean triplet - Problem 9
;;; problem-9.lisp
;;; John Niven
;;; 14 March 2021
;;; SOLVED: 31875000 (= 200 x 375 x 425)

;; A Pythagorean triplet is a set of three natural numbers, a < b < c, for
;; which,
;;          a^2 + b^2 = c^2
;; For example, 32 + 42 = 9 + 16 = 25 = 5^2.
;; There exists exactly one Pythagorean triplet for which a + b + c = 1000.
;; Find the product abc.

(in-package :coop.git.johnn.euler.problems)

(defun pythagorean-triplet-p (a b c)
  (= (+ (* a a) (* b b)) (* c c)))

(defun find-special-pythagorean-triplet (ubound)
  (let ((c 0))
    (loop for a from 1 to (- ubound 2) do
      (loop for b from (+ 1 a) to (- ubound 1) do
        (setq c (round (sqrt (+ (* a a) (* b b)))))
        (when (pythagorean-triplet-p a b c)
          (when (= (+ a b c) ubound)
            (return-from find-special-pythagorean-triplet (* a b c))))))))

(defun sandbox-9 ()
  (print "Expected result: 60")
  (find-special-pythagorean-triplet 12))

(defun problem-9 ()
  (print "Expected result: 31875000")
  (find-special-pythagorean-triplet 1000))
