;;; Sum square difference - Problem 6
;;; problem-6.lisp
;;; John Niven
;;; 14 March 2021
;;; SOLVED: 25164150

;; The sum of the squares of the first ten natural numbers is,
;;          1^2 + 2^2 + ... + 10^2 = 385
;; The square of the sum of the first ten natural numbers is,
;;          (1 + 2 + ... + 10)^2 = 55^2 = 3025
;; Hence the difference between the sum of the squares of the first ten natural
;; numbers and the square of the sum is 3025 - 385 = 2640.
;; Find the difference between the sum of the squares of the first one hundred
;; natural numbers and the square of the sum.

(in-package :coop.git.johnn.euler.problems)

(defun sum-squares (number)
  (let ((return-value 0))
    (loop for i from 1 to number do
        (setq return-value (+ return-value (* i i))))
    return-value))

(defun square-sums (number)
  (let ((return-value 0))
    (loop for i from 1 to number do
      (setq return-value (+ return-value i)))
    (setq return-value (* return-value return-value))
    return-value))

(defun sum-square-difference (number)
  (- (square-sums number) (sum-squares number)))

(defun sandbox-sum-square-difference ()
  (format t "~%sum-squares: ~a" (= 385 (sum-squares 10)))
  (format t "~%square-sums: ~a" (= 3025 (square-sums 10)))
  (format t "~%sum-square-difference: ~a" (= 2640 (sum-square-difference 10))))

; (sum-square-difference 100) => 25164150

(defun sandbox-6 ()
  (print "Exprected result: 2640")
  (sum-square-difference 10))

(defun problem-6 ()
  (print "Exprected result: 25164150")
  (sum-square-difference 100))
