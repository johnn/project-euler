;;; Lychrel numbers - Problem 55
;;; problem-55.lisp
;;; John Niven
;;; 13 March 2021
;;; SOLVED: 249

;; If we take 47, reverse and add, 47 + 74 = 121, which is palindromic.
;; Not all numbers produce palindromes so quickly. 349 [takes] three iterations
;; to arrive at a palindrome.
;; [I]t is thought that some numbers, like 196, never produce a palindrome. A
;; number that never forms a palindrome through the reverse and add process is
;; called a Lychrel number.
;; [W]e shall assume that a number is Lychrel [unless] it become[s] a palindrome
;; in less than fifty iterations.
;; How many Lychrel numbers are there below ten-thousand?

(defun palindromep (number)
    "Return T if number is a palindrome (e.g. 19791)."
    (equal (princ-to-string number) (reverse (princ-to-string number)))
)

(defun probably-lychrelp (number)
    (setf counter 1)
    (setf current-number number)
;    (format t "~%~a: " number)
;    (format t "~%(palindromep current-number): ~a" (palindromep current-number))
;    (format t "~%(>= counter 50): ~a" (>= counter 50))
    (loop while (< counter 50) do
        (setf current-number
                (+ current-number
                    (parse-integer
                        (reverse(princ-to-string current-number)))))
        (setf counter (+ 1 counter))
;        (format t "~%~a: ~a; " counter current-number)
        until (palindromep current-number))
    (not (palindromep current-number))
)

(defun test-lychrel-numbers ()
    (format t "~%195 (expected NIL): ~a" (probably-lychrelp 195))
    (format t "~%196 (expected T): ~a" (probably-lychrelp 196))
    (format t "~%4993 (expected NIL): ~a" (probably-lychrelp 4993))
    (format t "~%4994 (expected T): ~a" (probably-lychrelp 4994))
    T
)

(defun lychrel-numbers ()
    (setf lychrel-ctr 0)
    (loop for number from 1 to 9999 do
        (if (probably-lychrelp number)
            (progn
                (setf lychrel-ctr (+ 1 lychrel-ctr))
                (format t "~%~a is probably Lychrel. Count: ~a" number lychrel-ctr))
;            (format t "~%~a is not Lychrel." number)
        ))
    lychrel-ctr
)
