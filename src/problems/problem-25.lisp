;;; 1000-digit Fibonacci number - Problem 25
;;; problem-25.lisp
;;; John Niven
;;; 11 March 2021
;;; SOLVED: 4782

;; The Fibonacci sequence is defined by the recurrence relation:
;; Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
;; The 12th term, F12, is the first term to contain three digits.
;; What is the index of the first term in the Fibonacci sequence to contain 1000 digits?

(defun thousand-digit-fibonacci (maximum)
    (setf ctr 0)
    (setf term1 0)
    (setf term2 1)
    (setf term3 0)
    (loop while (<= (length (princ-to-string term1)) (- maximum 1)) do

        (setf ctr (+ 1 ctr))

        (setf term3 (+ term1 term2))
;        (format t "~%term3: ~a" term3)

        (setf term1 term2)
;        (format t "~%New term1: ~a" term1)
        (setf term2 term3)
;        (format t "~%New term2: ~a" term2)

;        (format t "~%~a :: term1: ~a , term2: ~a , term3: ~a" ctr term1 term2 term3)
    )
    ctr)
