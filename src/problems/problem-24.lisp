;;; Lexicographic permutations - Problem 24
;;; problem-24.lisp
;;; John Niven
;;; 22 March 2021
;;;

;; A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation
;; of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically or alphabetically,
;; we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
;;       012   021   102   120   201   210
;; What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?

(in-package :coop.git.johnn.euler.problems)

(defun sandbox-24 ()
  (let ((digits ()) (counter 1))
    (setq digits (list 0 1 2))
    (v:info '(:sandbox-24 :function-in) "@1: counter: ~a digits: ~a" counter digits)
    (loop do
      (setq counter (+ 1 counter))
      (setq digits (increment-permutation digits))
      (v:info :sandbox-24 "@2: counter: ~a digits: ~a" counter digits)
      while digits)))

(defun sandbox-24-2 ()
  (let ((digits ()) (counter 1))
    (setq digits (list 0 1 2 3))
    (v:info '(:sandbox-24-2 :function-in) "@1: counter: ~a digits: ~a" counter digits)
    (loop do
      (setq counter (+ 1 counter))
      (setq digits (increment-permutation digits))
      (v:info :sandbox-24-2 "@2: counter: ~a digits: ~a" counter digits)
      while digits)))

(defun problem-24 ()
  (let ((digits ()) (counter 1))
    (setq digits (list 0 1 2 3 4 5 6 7 8 9))
    (v:debug '(:problem-24 :function-in) "@1: ~a : ~a" counter digits)
    (loop do
      (setq counter (+ 1 counter))
      (setq digits (increment-permutation digits))
      (v:debug :problem-24 "@2: ~a : ~a" counter digits)
      while (and digits (<= counter 1000000)))))

(defun rotate (list i j)
  (rotatef (nth i list) (nth j list))
  list)

(defun rotate-v1 (list i j)
  (let ((list-tail (nthcdr i list)))
    (v:debug :rotate-v1 "PARAMS IN: list: ~a, i: ~a, j: ~a: " list i j)
    (v:debug :rotate-v1 "(nthcdr ~a ~a): ~a" i list (nthcdr i list))
    (v:debug :rotate-v1 "list-tail: " list-tail)
    (v:debug :rotate-v1 "(car list-tail): " (car list-tail))
    (rotatef (car (nthcdr i list))
             (elt (nthcdr i list) (- j i))
      list)))

(defun find-X (list)
  "Find the index of the first digit that can be incremented. For example, for a list (0 1 2 5 4 3)
  the first digit that can be incremented is 2, and its index is (also) 2."
  (v:debug '(:find-X :func-in) "list: ~a" list)
  (loop for i from 2 to (length list) do
    (if (< (elt list (- (length list) i))
           (elt list (+ 1 (- (length list) i))))
      (return-from find-X (- (length list) i)))))

;; TODO Tests are all passing, but problem-24 isn't working.
;; My guess is that the Y2 magic needs to be extended to Y3, Y4, etc.
;; I'd also guess that the smartest way to do this is here.
;; TODO x-value needs to be set once, rather than relying on a lookup [(elt list x-location)]
;; every iteration. This would also make it easier to apply the Y2, Y3...Yn change: we can
;; increment the target value if the (if (> x-location y-location) test fails.
(defun find-Y (list)
  "Find the index of the digit that is one greater than the first digit that can be incremented. For
  example, for a list (0 1 2 5 4 3) the first digit that can be incremented is 2. One greater than 2
  is 3, and 3's index is 5."
  (let ((x-location (find-X list)))
    (if (not x-location)
      nil
      (progn
        (v:debug :find-Y "@1. list: ~a, x-location: ~a" list x-location)
        (loop for i from 0 to (- (length list) 1) do
          (v:debug :find-Y "@2. list: ~a, x-location: ~a, i: ~a" list x-location i)
          (v:debug :find-Y "@3. (elt list ~a): ~a (+ 1 x-location): ~a" i (elt list i) (+ 1 x-location))
          (if (= (elt list i) (+ 1 (elt list x-location)))
            (progn
              (v:debug :find-Y "@4. list: ~a, RETURNING i: ~a" list i)
              (return-from find-Y i))))))))

(defun find-Y2 (list)
  "SMURGLE: Find the index of the digit that is one greater than the first digit that can be incremented. For
  example, for a list (0 1 2 5 4 3) the first digit that can be incremented is 2. One greater than 2
  is 3, and 3's index is 5."
  (let ((x-location (find-X list)))
    (if (not x-location)
      nil
      (progn
        (v:debug :find-Y2 "@1. list: ~a, x-location: ~a" list x-location)
        (loop for i from 0 to (- (length list) 1) do
          (v:debug :find-Y2 "@2. list: ~a, x-location: ~a, i: ~a" list x-location i)
          (v:debug :find-Y2 "@3. (elt list ~a): ~a (+ 1 x-location): ~a" i (elt list i) (+ 1 x-location))
          (if (= (elt list i) (+ 2 (elt list x-location)))
            (progn
              (v:debug '(:find-Y2 :function-return) "@4. list: ~a, RETURNING i: ~a" list i)
              (return-from find-Y2 i))))))))

(defun sort-digits (list position)
  (v:debug '(:sort-digits :function-in :function-return) "~a ~a ~a ~a" list position (subseq list 0 (+ 1 position)) (sort (nthcdr (+ 1 position) list) #'<))
  (append (subseq list 0 (+ 1 position)) (sort (nthcdr (+ 1 position) list) #'<)))

(defun increment-permutation (digits)
  (v:debug '(:increment-permutation :function-in) "@1. ~a. R: ~a ~a" digits (- (length digits) 2) (- (length digits) 1))
  (v:debug :increment-permutation "@1.1. ~a. R: ~a ~a" digits (elt digits (- (length digits) 2)) (elt digits (- (length digits) 1)))
  (if (< (elt digits (- (length digits) 2)) (elt digits (- (length digits) 1)))
    (progn
      (v:debug '(:increment-permutation.simple-case :function-return) "@2. ~a." digits)
      (rotate digits (- (length digits) 2) (- (length digits) 1)))
    (progn
      (v:debug :increment-permutation.complex-case "@3. ~a." digits)
      (let ((x-location (find-X digits)) (y-location (find-Y digits)) (y-value 0))
        (if (not y-location)
          (progn
            (v:debug '(:increment-permutation.complex-case.max :function-return) "@3. ~a" digits)
            nil)
          (progn
            (v:debug :increment-permutation.complex-case.rotate-and-sort "@3. ~a. y-loc: ~a" digits y-location)
            (if (> x-location y-location)
              (progn
                (v:debug :increment-permutation.complex-case.rotate-and-sort.Y2 "x-loc: ~a > y-loc: ~a SMURGLE" x-location y-location)
                (setq y-location (find-Y2 digits)))
              (v:debug :increment-permutation.complex-case.rotate-and-sort.Y "x-loc: ~a < y-loc: ~a" x-location y-location))
            (setq y-value (elt digits y-location))
            (v:debug :increment-permutation.complex-case.rotate-and-sort "@3.1. ~a. x-loc: ~a, y-loc: ~a, y-val: ~a" digits x-location y-location y-value)
            (v:debug :increment-permutation.complex-case.rotate-and-sort "(rotate ~a ~a ~a): " digits x-location y-location)
            (rotate digits x-location y-location)
            (v:debug '(:increment-permutation.complex-case.rotate-and-sort :function-return) "@3.2. ~a." digits)
            (sort-digits digits x-location)))))))
