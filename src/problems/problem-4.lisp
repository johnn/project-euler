;;; Largest palindrome product - Problem 4
;;; problem-4.lisp
;;; John Niven
;;; 13 March 2021
;;; SOLVEDD: 906609

;; A palindromic number reads the same both ways. The largest palindrome made
;; from the product of two 2-digit numbers is 9009 = 91 × 99.
;; Find the largest palindrome made from the product of two 3-digit numbers.

(in-package :coop.git.johnn.euler.problems)

(defun problem-4 ()
  (get-largest-palindrome-product 999))

(defun sandbox-4 ()
  (print "Expected result: 9009")
  (get-largest-palindrome-product 99))

(defun get-largest-palindrome-product (maximum)
  (let ((product 0) (max-product 0))
    (loop for outer from 1 to maximum do
      (loop for inner from outer to maximum do
        (setf product (* inner outer))
        (if (and (palindromep product) (> product max-product))
          (setf max-product product))))
    max-product))
