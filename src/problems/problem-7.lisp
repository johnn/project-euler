;;; 10001st prime - Problem 7
;;; problem-7.lisp
;;; John Niven
;;; 14 March 2021
;;; SOLVED: 104743

;; By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
;; that the 6th prime is 13.
;; What is the 10,001st prime number?

(in-package :coop.git.johnn.euler.problems)

(defun find-nth-prime (number)
  (let ((primes-counter 1) (try 1))
    (loop while (< primes-counter number) do
      (setq try (+ 2 try))
      (when (primep try)
        (setq primes-counter (+ 1 primes-counter))))
    try))

(defun sandbox-7 ()
  (print "Expected result: 13")
  (find-nth-prime 6))
