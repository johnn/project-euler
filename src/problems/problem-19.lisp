;;; Counting Sundays - Problem 19
;;; problem-19.lisp
;;; John Niven
;;; 11 March 2021
;;; SOLVED: 171

;; You are given the following information, but you may prefer to do some
;; research for yourself.
;;
;; 1 Jan 1900 was a Monday.
;; Thirty days has September,
;; April, June and November.
;; All the rest have thirty-one,
;; Saving February alone,
;; Which has twenty-eight, rain or shine.
;; And on leap years, twenty-nine.
;;
;; A leap year occurs on any year evenly divisible by 4, but not on a century
;; unless it is divisible by 400.
;; How many Sundays fell on the first of the month during the twentieth century
;; (1 Jan 1901 to 31 Dec 2000)?

(defun length-of-february (year)
    (if
        (and
            (= (mod year 4) 0)
            (or (> (mod year 100) 0) (= (mod year 400) 0)))
        29
        28))

(defun length-of-month (year month)
    (cond
        ((or (= month 4) (= month 6) (= month 9) (= month 11))
                30)
        ((or (= month 1) (= month 3) (= month 5) (= month 7) (= month 8) (= month 10) (= month 12))
                31)
        ((and (= month 2))
                (length-of-february year))))

(defun count-sundays ()
    (setf previous-days 0)
    (setf sunday-counter 0)
    (loop for year from 1900 to 2000 do
        (loop for month from 1 to 12 do
            (if (= (mod (+ 1 previous-days) 7) 0)
                (progn
                    (format t "~%~a-~a-01: ~a: ~a: ~a: ~a" year month (length-of-month year month) (+ 1 previous-days) (mod (+ 1 previous-days) 7) (+ 1 sunday-counter))
                    (if (>= year 1901)
                        (setf sunday-counter (+ 1 sunday-counter)))))
            (setf previous-days
                (+ (length-of-month year month) previous-days))))
    sunday-counter)
