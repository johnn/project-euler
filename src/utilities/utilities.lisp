;;; Project Euler utilities
;;; euler-utilities.lisp
;;; John Niven
;;; 18 March 2021

(in-package :coop.git.johnn.euler.utilities)

(defun generate-nth-triangular-number (n)
  (/ (* n (+ 1 n)) 2))

(defun get-factors (number)
  (let ((factors (list 1)))
    (loop for factor from 2 to (floor (sqrt number)) do
      (when (= (mod number factor) 0)
        (progn
          (push factor factors)
          (when (/= factor (/ number factor))
            (push (/ number factor) factors)))))
    (when (> number 1)
      (push number factors))
    (sort factors #'<)))

(defun get-prime-factors (number)
  (let ((factors ()))
    (dolist (factor (get-factors number)) do
      (when (primep factor)
        (push factor factors)))
;    (format t "~%~a: number: ~a, factors: ~a" funcname number (sort factors #'<))
    (sort factors #'<)))

(defun power (m n)
  (let ((result 1))
    (dotimes (count n result)
      (setf result (* m result)))))

(defun divides-evenly-p (number divisor)
  (= (mod number divisor) 0))

(defun primep (number)
  (if (<= number 1)
    (return-from primep nil)
    (if (= number 2)
      (return-from primep T)
      (if (= (mod number 2) 0)
        (return-from primep nil)
        (progn
          (let ((counter 3))
            (loop while (<= (* counter counter) number) do
              (if (= (mod number counter) 0)
                (return-from primep nil)
                (setq counter (+ 2 counter)))))
          (return-from primep T))))))

(defun factorial (n)
  (cond
    ((= n 0) 1) ;; Special case, 0! = 1
    ((= n 1) 1) ;; Base case, 1! = 1
    (t
      ;; Recursive case
      ;; Multiply n by the factorial of n - 1.
      (* n (factorial (- n 1))))))

(defun palindromep (number)
  "Return T if number is a palindrome (e.g. 19791)."
  (equal (princ-to-string number) (reverse (princ-to-string number))))

(defun add-string-digits (number-string)
  (let ((sum 0))
    (loop for i from 0 to (- (length number-string) 1) do
      (setq sum (+ sum (parse-integer (subseq number-string i (+ 1 i))))))
    sum))
