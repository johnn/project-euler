# Project Euler

My attempt at tackling Project Euler using (and learning) Common Lisp.

# Overview

* [Diary](diary.md)

## Useful links

### Project

* Project Euler: https://projecteuler.net/progress
* git.coop: https://git.coop/johnn/project-euler
* GitLab Markdown cheatsheet: https://gitlab.com/francoisjacquet/rosariosis/-/wikis/Markdown-Cheatsheet

### Common Lisp

* Style guide: https://lisp-lang.org/style-guide/
* Cookbook: http://cl-cookbook.sourceforge.net/index.html
* A road to Common Lisp: https://stevelosh.com/blog/2018/08/a-road-to-common-lisp/
* ASDF (build tool): https://common-lisp.net/project/asdf/asdf.pdf
* Quicklisp (library manager): https://www.quicklisp.org/beta/
* Lisp-unit (regression/unit testing): https://github.com/OdonataResearchLLC/lisp-unit/wiki
* Verbose (logging): https://shinmera.github.io/verbose/

### Git

* Git: https://git-scm.com/book/en/v2

# Cheatsheet

## Common Lisp

### Workflow: load, test, and quit

#### Simple, complete

* `(load "t/test-runner.lisp")`

#### Detailed

* `(asdf:load-system :project-euler)`
* `(asdf:load-system :project-euler/test)`


* *`(in-package :coop.git.johnn.euler.utilities.tests)`* ...or... *`(in-package :coop.git.johnn.euler.problems.tests)`*


* *`(run-tests :all)`*
* `(print-errors *)` ...or... `(print-failures *)`


* `(in-package :cl-user)`
* `(quit)`

##### Run individual test

* `(asdf:load-system :project-euler)`
* `(asdf:load-system :project-euler/test)`


* *`(in-package :coop.git.johnn.euler.problems.tests)`*
* *`(run-tests '(test-problem-1))`*
* `(print-errors *)` ...or... `(print-failures *)`


* `(in-package :cl-user)`
* `(quit)`

##### Run tagged tests

* `(asdf:load-system :project-euler)`
* `(asdf:load-system :project-euler/test)`


* *`(in-package :coop.git.johnn.euler.utilities.tests)`*
* *`(run-tags '(:prime))`*
* `(print-errors *)` ...or... `(print-failures *)`


* *`(in-package :coop.git.johnn.euler.problems.tests)`*
* *`(run-tags '(:problem-24))`*
* `(print-errors *)` ...or... `(print-failures *)`


* `(in-package :cl-user)`
* `(quit)`

##### Run individual problem

* `(coop.git.johnn.euler.problems::problem-1)`

## Git

* `git pull https://git.coop/johnn/project-euler.git` ...or... `git pull origin`
* `git push -u origin --all` (use `-f` or `--force` sparingly to get out of trouble)

# License

Copyright (c) 2021 John Niven

Licensed under the GPL, v3.
